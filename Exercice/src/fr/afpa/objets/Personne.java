package fr.afpa.objets;

import java.util.Arrays;

public class Personne { 
	
private String nom;
private String prenom;
private Adresse[] tabAdresse;
private int age;



public Personne(String nom, String prenom, Adresse[] tabAdresse, int age) {
	super();
	this.nom = nom;
	this.prenom = prenom;
	this.tabAdresse = tabAdresse;
	this.age = age;
	this.tabAdresse=new Adresse[3];
	
	for (int i= 0; i<tabAdresse.length;i++) {
		this.tabAdresse[i]=new Adresse();
	
	}
}



public Personne(String nom, String prenom, int age) {
	super();
	this.nom = nom;
	this.prenom = prenom;
	this.age = age;
	this.tabAdresse=new Adresse[3];
	for (int i= 0; i<tabAdresse.length;i++) {
		this.tabAdresse[i]=new Adresse();
	
	}
}






public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}
public Adresse[] getTabAdresse() {
	return tabAdresse;
}
public void setTabAdresse(Adresse[] tabAdresse) {
	this.tabAdresse = tabAdresse;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
@Override
public String toString() {
	return "Personne [nom=" + nom + ", prenom=" + prenom + ", tabAdresse=" + Arrays.toString(tabAdresse) + ", age="
			+ age + "]";
}

	

}
