package fr.afpa.objets;

public class Pays {
private String codePays;
private String nom;
public Pays(String codePays, String nom) {
	super();
	this.codePays = codePays;
	this.nom = nom;
}
public String getCodePays() {
	return codePays;
}
public void setCodePays(String codePays) {
	this.codePays = codePays;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
@Override
public String toString() {
	return "Pays [codePays=" + codePays + ", nom=" + nom + "]";
}

}
