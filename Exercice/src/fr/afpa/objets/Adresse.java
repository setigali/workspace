package fr.afpa.objets;

public class Adresse {
	private String rue;
	private String cp;
	private String ville;
	private Pays pays;
	private int numero;
	
	
	public Adresse(String rue, String cp, String ville, Pays pays, int numero) {
		super();
		this.rue = rue;
		this.cp = cp;
		this.ville = ville;
		this.pays = pays;
		this.numero = numero;
	
		

	}
	
	
	
	public Adresse() {
		super();
	}



	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public Pays getPays() {
		return pays;
	}
	public void setPays(Pays pays) {
		this.pays = pays;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}


	@Override
	public String toString() {
		return "Adresse [rue=" + rue + ", cp=" + cp + ", ville=" + ville + ", pays=" + pays + ", numero=" + numero
				+ "]";
	} 
	
	
	

}
