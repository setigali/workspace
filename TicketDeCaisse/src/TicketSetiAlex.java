import java.util.Scanner;

public class TicketSetiAlex {



public static void main(String[] args) {
		// TODO Auto-generated method stub
	
	Scanner in=new Scanner(System.in);	
		String code="";
		String question="";
		String tvasansvirg="";
		
		int totalq=0;
		int occhoco=0;
		int oclait=0;
		int ocyaourt=0;
		int ocordi=0;
		int occhaise=0;
		
		boolean encore=true;
		boolean pp=false; //petit pain au chocolat
		boolean l=false; //lait
		boolean y=false; //yaourt
		boolean ord=false; //ordinateur
		boolean ch=false;//chaise
		
		float prixchoco=0;
        float prixlait=0;
        float prixyaourt=0;
        float prixchaise=0;
        float prixordi=0;
        float totalprix=0;
        float tvaavecvirgule=0;
        
        
	while(encore) {
		System.out.println("Saisissez le code produit: ");
		code=in.next();
		in.nextLine();
		totalq++;
		
		// ici on incremente les quantites specifiques aux produits s'ils sont scann�s	
		if(code.equals("0015154300")) { 
			pp=true;
			occhoco++;
			prixchoco= (float) (prixchoco +1.67);
		}
		
		else if(code.equals("0016078185")) { 
			l=true;
			oclait++;
			prixlait= (float) (prixlait+1.99);
		}
		
		else if(code.equals("0016064546")) { 
			y=true;
			ocyaourt++;
			prixyaourt= (float) (prixyaourt+1.5);
		}
		
		else if(code.equals("0016084193")) { 
			ch=true;
			occhaise++;
			prixchaise=(float) (prixchaise+49.9);
		}
		
		else if(code.equals("0016058844")) { 
			ord=true;
			ocordi++;
			prixordi=(float) (prixordi+599.99);
		}
		else {
			System.out.println("Erreur");
			totalq--;
		}
		
		// si il y a d'autres produits, on continue sinon on sort de la boucle
		
		while (true){ 
		System.out.println("Avez vous un autre produit � scanner? O/N");
		question=in.next();
		in.nextLine();
		
		if("O".equals(question)) {
				encore=true;
				break;
			}
		else if("N".equals(question)){
				encore=false;
				System.out.println("Fin de la transaction");
				System.out.println("");
				in.close();
				break;
				}
		else {
			System.out.println("Erreur");
		}
		}
	
	}
// Affichage Ticket en fonction s'ils ont ete scann�es ou pas
	System.out.println("\t\t      AFPA\n \t\t TICKET ALEX SETI\n \t\t  03 00 00 00 00\n \n");
System.out.println("Article           Quantit�       Prix");	
System.out.println(" ");
System.out.println(" ");
	if(pp) {
		System.out.println("Pain au chocolat     "+occhoco+"           "+"1,67");
		System.out.println(" ");
	}

	if(l) {
		System.out.println("Lait                 "+oclait+"           "+"1,99");
		System.out.println(" ");
	}
	if(y) {
		System.out.println("Yaourt               "+ocyaourt+"           "+"1,5");
		System.out.println(" ");
	}
	if(ch) {
		System.out.println("Chaise        	     "+occhaise+"           "+"49,9");
		System.out.println(" ");
	}
	if(ord) {
		System.out.println("Ordinateur           "+ocordi+"           "+"599,99");
		System.out.println(" ");
	}

//calcul du prix total
	
	totalprix=(prixchoco+prixlait+prixyaourt+prixordi+prixchaise);
	
//on transforme le "." en ","
	
	tvaavecvirgule=(float) (0.2*totalprix);
	tvasansvirg=virguleGenerale(tvaavecvirgule);
	
	//on continue l'affichage du total 
ligne();

System.out.println("Total                            "+deuxChiffres(virguleGenerale(totalprix))+" euros");

ligne();

System.out.println("20% - TVA                        "+deuxChiffres(tvasansvirg));
System.out.println(" ");
ligne();
// pour enlever le "s" si un seul article est scann� 
if(totalq==1) {
System.out.println("                     "+totalq+" Article");
}
else {
System.out.println("                     "+totalq+" Articles");
}

ligne();
System.out.println("Date     : 19/11/2013     10:15");

}

//methode pour transformer le "." en ","
static String virguleGenerale(float a) {
	String virgule="";
	virgule=""+a;
	virgule=virgule.replaceAll("\\.", ",");
	return virgule;
}

//pour avoir deux chiffres apr�s la virgule 
static String deuxChiffres(String chaine) {
	String test="";
	int i = 0;
	while (i<=chaine.length()) {
		if(chaine.charAt(i)==',') {
			i=i+3;
			break;
		}
	i++;
	}
	test=chaine.substring(0,i);
	return test;	
	}

//pour afficher les lignes 
static void ligne() {
	for (int i = 0; i < 50; i++) {
		System.out.print("-");
		}
	System.out.println(" ");
}
}