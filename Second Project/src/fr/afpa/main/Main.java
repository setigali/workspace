package fr.afpa.main;

import fr.afpa.objets.Radar;
import fr.afpa.objets.Voiture;

public class Main {

	public static void main(String[] args) {

		Voiture v = new Voiture("Afpa", "rouge", "R00151563", 50, 130);
		Voiture a = new Voiture("cristaline", "bleu", "R00151663", 124, 200);
		Voiture b = new Voiture("Evian", "verte", "R00151763", 124, 240);
		
		Radar radar= new Radar(120);
		
		v.accelerer(20);
		v.afficherInfos();
		a.afficherInfos();
		b.afficherInfos();
		
		a.demarrer();
		a.accelerer(100);
		a.afficherInfos();
		
		radar.flash(b);
		b.demarrer();
		
		b.afficherInfos();

	
		b.afficherInfos();
		radar.flash(b);
	}

}
