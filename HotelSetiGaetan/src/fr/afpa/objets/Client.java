package fr.afpa.objets;

import java.util.Scanner;

public class Client {

	private String nomClient;
	private String prenomClient;
	private String emailClient;
	private String identifiantClient;
	
	public Client(String nomClient1, String prenomClient1, String emailClient1,String identifiantClient1) {
		
		setNomClient(nomClient1);
		setPrenomClient(prenomClient1);
		setEmailClient(emailClient1);
		setIdentifiantClient(identifiantClient1);
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient1) {
		nomClient = nomClient1;
	}

	public String getPrenomClient() {
		return prenomClient;
	}

	public void setPrenomClient(String prenomClient1) {
		prenomClient = prenomClient1;
	}

	public String getEmailClient() {
		return emailClient;
	}

	public void setEmailClient(String emailClient1) {
		emailClient = emailClient1;
	}

	public String getIdentifiantClient() {
		return identifiantClient;
	}

	public void setIdentifiantClient(String identifiantClient1) {
		identifiantClient = identifiantClient1;
	}

	@Override
	public String toString() {
		return "Client [nomClient=" + nomClient + ", prenomClient=" + prenomClient + ", emailClient=" + emailClient
				+ ", identifiantClient=" + identifiantClient + "]";
	}
	
	
	
	public static void creationCompteClient(){
		Scanner in = new Scanner(System.in);
		String nom = "";
		String prenom = "";
		System.out.println("Vous allez cr�er votre compte client");
		System.out.println("Veuillez saisir votre nom");
		nom=in.nextLine();
		System.out.println("Veuillez saisir votre prenom");
		prenom=in.nextLine();
		Controle.validLoginClient();
	}
}
