package fr.afpa.objets;

public class Chambre {

private String typeChambre;
private String taille;
private String vues;
private String tarif;
private String options;

	

public Chambre(String type, String surface, String panorama, String prix, String choixOptions) {
	setTypeChambre(type);
	setTaille(surface);
	setVues(panorama);
	setTarif(prix);
	setOptions(choixOptions);
}


public String getTypeChambre() {
	return typeChambre;
}


public void setTypeChambre(String type) {
	typeChambre = type;
}


public String getTaille() {
	return taille;
}


public void setTaille(String surface) {
	taille = surface;
}


public String getVues() {
	return vues;
}


public void setVues(String panorama) {
	vues = panorama;
}


public String getTarif() {
	return tarif;
}


public void setTarif(String prix) {
	tarif = prix;
}



public String getOptions() {
	return options;
}


public void setOptions(String choixOptions) {
	options = choixOptions;
}


@Override
public String toString() {
	return "Chambre [typeChambre = " + typeChambre + ", taille = " + taille + ", vues = " + vues + ", tarif = " + tarif+"�"
			+ ", options = " + options + "]";
}

}