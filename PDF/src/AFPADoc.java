import java.io.FileNotFoundException;
import java.net.MalformedURLException;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TabAlignment;
import com.itextpdf.layout.property.TextAlignment;

public class AFPADoc {

	private static final Color Green = null;

	public static void main(String[] args) {
		
		
	String chemin = "C:\\ENV\\Ressources\\PDF\\exopdf.pdf";
	PdfWriter writer=null;
	try {
		writer = new PdfWriter(chemin);
	} catch (FileNotFoundException e1) {
		
		e1.printStackTrace();}

	PdfDocument pdf = new PdfDocument(writer);
	
	Document document = new Document(pdf,PageSize.A4.rotate());
	
	
    
    
    
	document.add(new Paragraph ("\n\n\n\n\n\n\n\n\n\nCENTRE AFPA ROUBAIX\n"
			+ "20 Rue du Luxembourg - 59100 ROUBAIX\n").setTextAlignment(TextAlignment.CENTER));	
	
	
	document.add (new Paragraph ("LISTE DES SAUVETEURS SECOURISTES DU CENTRE").setUnderline().setTextAlignment(TextAlignment.CENTER));
	
	document.add (new Paragraph (""));
	Table tableau = new Table(2).setBorder(new SolidBorder(ColorConstants.GREEN, 1));
	tableau.setWidth(700);
	
	
	tableau.addHeaderCell("NOM/PRENOM").setBackgroundColor(Green, 0);
	tableau.addHeaderCell("N� T�l�phone").setBackgroundColor(Green);
	
	tableau.addCell(new Cell().add(new Paragraph("AKLIL MALIKA")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("06 81 74 81 32")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("ARNOUT CINDY")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("06 02 07 01 63")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("CHILAH DJAMILA")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("06 32 76 84 44")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("HUJEUX PASCAL")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("06 08 41 63 84")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("ROSELLE NANCY")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("07 84 42 47 43")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("ZEHAR HADJI")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	tableau.addCell(new Cell().add(new Paragraph("06 32 76 65 37")).setBorder(new SolidBorder(ColorConstants.GREEN, 1)));
	
	
	document.add(tableau);
	
	
	
	String cheminImage= "C:\\ENV\\Ressources\\Images\\AFPA-logo.png";
	Image image = null;
	try {
		image = new Image(ImageDataFactory.create(cheminImage));
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	document.add(image.setFixedPosition(20,500));
	image.setHeight(2).setWidth(2);
	
	
	String cheminImage2= "C:\\ENV\\Ressources\\Images\\pompiers.png";
	Image image2 = null;
	try {
		image2 = new Image(ImageDataFactory.create(cheminImage2));
		image2.setHeight(100).setWidth(800);
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	document.add(image2.setFixedPosition(25, 400));
	
	
	document.close();
	}

	
}

