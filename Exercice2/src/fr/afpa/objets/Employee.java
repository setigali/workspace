package fr.afpa.objets;

public class Employee extends Personne {

	private String numEmployee;
	private double salaire;
	
	
	
	
	public Employee(String numEmployee, double salaire,String nom,String prenom) {
		super(nom, prenom);
		this.numEmployee = numEmployee;
		salaire = salaire;
	}




	public String getNumEmployee() {
		return numEmployee;
	}




	public void setNumEmployee(String numEmployee) {
		this.numEmployee = numEmployee;
	}




	public double getSalaire() {
		return salaire;
	}




	public void setSalaire(float salaire) {
		salaire = salaire;
	}
	
	public void sePresenter() {
		System.out.println("Bonjour. Je suis" + getNom() + " "+ getPrenom());
	}
	
	public void calculPrime() {
		double prime;
		double salaire = 0;
		
		prime=(salaire*0.05);
		
		System.out.println(prime);

	}




	@Override
	public String toString() {
		return "Employee [numEmployee=" + numEmployee + ", salaire=" + salaire + "]";
	}
	
	
}
