package fr.afpa.objets;


import java.util.Scanner;

public class Personne {
	
	private String nom;
	private String prenom;
	private String DateNaissance;
	private String email;
	private String numTelephone;
	private int distance;
	private String numRue;
	private String libelleRue;
	private String codePostal;
	private String ville;
	private String nomPays;
	private String codePays;
	
	
	public Personne() {
		super();
	}


	public Personne(String nom, String prenom, String dateNaisance, String email, String numTelephone, int distance,
			String numRue, String libelleRue, String codePostal, String ville, String nomPays, String codePays) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.DateNaissance = dateNaisance ;
		this.email = email;
		this.numTelephone = numTelephone;
		this.distance = distance;
		this.numRue = numRue;
		this.libelleRue = libelleRue;
		this.codePostal = codePostal;
		this.ville = ville;
		this.nomPays = nomPays;
		this.codePays = codePays;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getDateNaissance() {
		return DateNaissance;
	}


	public void setDateNaissance(String dateNaissance) {
		DateNaissance = dateNaissance;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getNumTelephone() {
		return numTelephone;
	}


	public void setNumTelephone(String numTelephone) {
		this.numTelephone = numTelephone;
	}


	public int getDistance() {
		return distance;
	}


	public void setDistance(int distance) {
		this.distance = distance;
	}


	public String getNumRue() {
		return numRue;
	}


	public void setNumRue(String numRue) {
		this.numRue = numRue;
	}


	public String getLibelleRue() {
		return libelleRue;
	}


	public void setLibelleRue(String libelleRue) {
		this.libelleRue = libelleRue;
	}


	public String getCodePostal() {
		return codePostal;
	}


	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}


	public String getVille() {
		return ville;
	}


	public void setVille(String ville) {
		this.ville = ville;
	}


	public String getNomPays() {
		return nomPays;
	}


	public void setNomPays(String nomPays) {
		this.nomPays = nomPays;
	}


	public String getCodePays() {
		return codePays;
	}


	public void setCodePays(String codePays) {
		this.codePays = codePays;
	}
	
	
	
	
	
	
	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", DateNaissance=" + DateNaissance + ", email=" + email
				+ ", numTelephone=" + numTelephone + ", distance=" + distance + ", numRue=" + numRue + ", libelleRue="
				+ libelleRue + ", codePostal=" + codePostal + ", ville=" + ville + ", nomPays=" + nomPays
				+ ", codePays=" + codePays + "]";
	}


	public Personne creerPersonne(Personne personne) {
		Scanner in = new Scanner (System.in);
		
		System.out.println("Veuillez saisie le nom");
		nom = in.nextLine();
		
		System.out.println("Veuillez saisir le prenom");
		prenom = in.nextLine();
		
		System.out.println("Veuillez saisir l'adresse mail");
		email= in.nextLine();
		
		System.out.println("Veuillez saisir la date de naissance");
		DateNaissance=in.nextLine();
		
		System.out.println("Veuillez saisir le numero de telephone");
		/*boolean saisie = false;
		
		while (saisie == false) {
			if (numTelephone.length() == 10) {
				for (int i = 0; i < numTelephone.length(); i++) {

					if (Character.isDigit(numTelephone.charAt(i))) {
						saisie = true;
						
					} else {
						saisie = false;
						System.out.println("Code invalide, veuillez retaper un code");*/
						numTelephone = in.nextLine();
						
					
		
		System.out.println("Veilez saisir la distance");
		distance=in.nextInt();
		
		System.out.println("Veuillez saisir le numero de la rue");
		numRue=in.nextLine();
		
		System.out.println("Veuillez saisir le libelle de la rue");
		libelleRue=in.nextLine();
		
		System.out.println("Veuillez saisir le codePostal");
		codePostal=in.nextLine();
	/*	boolean saisie2 = false;

		while (saisie2 == false) {
			if (codePostal.length() == 5) {
				for (int j = 0; j < codePostal.length(); j++) {
					if (Character.isDigit(codePostal.charAt(j))) {
						saisie2 = true;
					} else {
						saisie2 = false;
						System.out.println("Code invalide, veuillez retaper un code");*/
						codePostal = in.nextLine();
						
				
		
		
		System.out.println("Veuillez saisir la ville");
		ville= in.nextLine();
		
		System.out.println("Veuillez saisir le nom du pays");
		nomPays=in.nextLine();
		
		System.out.println("Veuillez saisir le code pays");
		codePays= in.nextLine();
		
		
		
				
	
		return new Personne(nom, prenom, DateNaissance, email, numTelephone, distance, numRue, libelleRue, codePostal, ville, nomPays, codePays);}
			
		
public void afficher() {
	System.out.println("Veuillez saisir le nombre de personnes � afficher");
}



}
	

