package qrcodeMain;

import java.io.IOException;
import java.util.Scanner;

import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import qrcodeObjets.QRCcodeGenerator;

public class QRCodeGeneratorMain {

	public static void main(String[] args) throws WriterException, IOException {
Scanner in =new Scanner(System.in);
String motacoder;
String nomfichier="";		
	System.out.println("Saisir les donn�es � coder");	
	motacoder=in.nextLine();
	System.out.println("Saisir nom du fichier");
	nomfichier= in.nextLine();
	
	System.out.println("Saisir la taile du fichier");
		int a = in.nextInt();
		in.nextLine();
	BitMatrix bm = QRCcodeGenerator.generateMatrix(motacoder,a);
	
	QRCcodeGenerator.writeImage(bm, nomfichier);

	}

}
