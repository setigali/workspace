package fr.afpa.objets;

public class Personne {

	private int id;
	private String nom;
	private String prenom;
	private String email;
	private String telephone;
	private double salaire;
	static int lastId;
	

	
	
	
		
		
	public Personne(String nom, String prenom, String email, String telephone, double salaire) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.salaire = salaire;
	
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public double getSalaire() {
		return salaire;
	}

	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public double calculSalaire() {
		return salaire;
	}
	public void afficher() {
		System.out.println("Id :"+this.id);
		System.out.println("Nom :"+this.nom);
		System.out.println("Prenom :"+this.prenom);
		System.out.println("Mail :"+this.getEmail());
		System.out.println("Telephone :"+this.telephone);
		System.out.println("Salaire :"+this.calculSalaire());
	}

}
