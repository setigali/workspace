package fr.afpa.objets;

public class Profil extends Utilisateur {

	private int id;
	private String code;
	private String libelle;
	private static int lastId=0;
	
	
	
	public Profil(String nom, String prenom, String email, String telephone, double salaire, String login,
			String password, String service, Profil profil, String code, String libelle) {
		super(nom, prenom, email, telephone, salaire, login, password, service, profil);
		this.code = code;
		this.libelle = libelle;
	}









	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}




	@Override
	public String toString() {
		return "Profil [id=" + id + ", code=" + code + ", libelle=" + libelle + "]";
	}

	
	


	





}
