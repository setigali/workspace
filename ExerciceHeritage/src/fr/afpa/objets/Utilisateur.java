package fr.afpa.objets;

public class Utilisateur extends Personne {

	
	private String login;
	private String password;
	private String service;
	private Profil profil;
	

	

	

	public Utilisateur(String nom, String prenom, String email, String telephone, double salaire, String login,
			String password, String service, Profil profil) {
		super(nom, prenom, email, telephone, salaire);
		this.login = login;
		this.password = password;
		this.service = service;
		this.profil = profil;
	}

	



public String getLogin() {
		return login;
	}



	public void setLogin(String login) {
		this.login = login;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public Profil getProfil() {
		return profil;
	}

	public void setProfil(Profil profil) {
		this.profil = profil;
	}





public double calculSalaire() {
	
	double nouveauSalaire = super.calculSalaire();
	
	if ("MN".equals(profil.getCode())) {
		return nouveauSalaire*0.1;
	}
	
	if ("DG".equals(profil.getCode())) {
		return nouveauSalaire*0.4;
		}
	
	return super.calculSalaire();
}

public void afficher() {
	super.afficher();
	System.out.println("Login :"+this.login);
	System.out.println("Password :"+this.password);
	System.out.println("Service :"+this.service);
	System.out.println(profil);
}


}



