package main.emploidutemps;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.DoubleBorder;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

import emploidutemps.objets.SaisieDonnees;

public class Main {

	public static void main(String[] args) {

		String formateur = SaisieDonnees.saisieFormateur();
		String telephone = SaisieDonnees.saisieTelephone();
		String fax = SaisieDonnees.saisieFax();
		String groupe = SaisieDonnees.saisieGroupe();
		String salle = SaisieDonnees.saisieGroupe();
		String dateDebut = SaisieDonnees.saisieDate();
		String dateFin = SaisieDonnees.saisieDate();

		String chemin = "C:\\ENV\\Ressources\\PDF\\tableau Julien.pdf";
		PdfWriter writer = null;
		try {
			writer = new PdfWriter(chemin);
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
		}

		PdfDocument pdf = new PdfDocument(writer);

		Document document = new Document(pdf, PageSize.A4.rotate());

		document.add(new Paragraph("Centre AFPA de ROUBAIX").setTextAlignment(TextAlignment.LEFT).setFontSize(10));

		document.add(new Paragraph("Groupe " + groupe + "\t\tdu " + dateDebut + "\t\tau " + dateFin+"\nSalle "+salle).setBold()
				.setTextAlignment(TextAlignment.CENTER));

		document.add(new Paragraph("Formateur : " + formateur + "\nT�l : " + telephone + "\nFax :" + fax)
				.setTextAlignment(TextAlignment.LEFT).setFontSize(10));

		String cheminImage = "C:\\ENV\\Ressources\\Images\\logo2.png";
		Image image = null;
		try {
			image = new Image(ImageDataFactory.create(cheminImage));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		document.add(image.setFixedPosition(550, 500));
		image.setHeight(2).setWidth(2);

		document.add(new Paragraph().setTextAlignment(TextAlignment.LEFT));

		// Cr�ation du premier tableau
		Table preTableau = new Table(7).setWidth(750);
		// fusion de la premiere ligne
		preTableau.addCell(new Cell(1, 7).add(new Paragraph(SaisieDonnees.saisieFormation()).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		
		// fusion de la cellule avec celle d'en dessous
		preTableau.addCell(
				new Cell(2, 1).add(new Paragraph("P�riode")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(
				new Cell(1, 2).add(new Paragraph("MATIN")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("")).setBackgroundColor(ColorConstants.LIGHT_GRAY)
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(
				new Cell(1, 2).add(new Paragraph("APRES MIDI")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		
		// fusion de la cellule avec celle d'en dessous
		preTableau.addCell(new Cell(2, 1).add(new Paragraph("Dur�e quotidienne"))
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("d�but")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("fin")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("")).setBackgroundColor(ColorConstants.LIGHT_GRAY)
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("d�but")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("fin")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		
		
		// 1er jour : Lundi
		String[] horaireJournalier = SaisieDonnees.saisieHoraire();
		preTableau.addCell(new Cell().add(new Paragraph("Lundi")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[0]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[1]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("")).setBackgroundColor(ColorConstants.LIGHT_GRAY)
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[2]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[3]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("7h")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)).setBold());
		
		// 2e jour : Mardi
		horaireJournalier = SaisieDonnees.saisieHoraire();
		preTableau.addCell(new Cell().add(new Paragraph("Mardi")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[0]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[1]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("")).setBackgroundColor(ColorConstants.LIGHT_GRAY)
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[2]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[3]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("7h")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)).setBold());
		
		// 3e jour : Mercredi
		horaireJournalier = SaisieDonnees.saisieHoraire();
		preTableau
				.addCell(new Cell().add(new Paragraph("Mercredi")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[0]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[1]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("")).setBackgroundColor(ColorConstants.LIGHT_GRAY)
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[2]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[3]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("7h")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)).setBold());
		
		// 3e jour : Mercredi
		horaireJournalier = SaisieDonnees.saisieHoraire();
		preTableau.addCell(new Cell().add(new Paragraph("Jeudi")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[0]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[1]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("")).setBackgroundColor(ColorConstants.LIGHT_GRAY)
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[2]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[3]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("7h")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)).setBold());
		
		
		// 3e jour : Mercredi
		horaireJournalier = SaisieDonnees.saisieHoraire();
		preTableau
				.addCell(new Cell().add(new Paragraph("Vendredi")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[0]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[1]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("")).setBackgroundColor(ColorConstants.LIGHT_GRAY)
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[2]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph(horaireJournalier[3]).setBold())
				.setBorder(new SolidBorder(ColorConstants.BLACK, 1)));
		preTableau.addCell(new Cell().add(new Paragraph("7h")).setBorder(new SolidBorder(ColorConstants.BLACK, 1)).setBold());
		
		
		// ajout du premier tableau
		document.add(preTableau.setTextAlignment(TextAlignment.CENTER).setFontSize(8));

		document.add(new Paragraph( "                                                                                        35h00 ").setFontSize(8).setTextAlignment(TextAlignment.RIGHT).setMarginRight(200).setBold());
		
		
		
		// Cr�ation du dernier tableau
		String election = SaisieDonnees.saisieDate();
		String rencontre = SaisieDonnees.saisieDate();
		String sensibilisation = SaisieDonnees.saisieDate();
		String atelier = SaisieDonnees.saisieDate();
		String espaces = SaisieDonnees.saisieDate();
		String conged1 = SaisieDonnees.saisieDate();
		String congef1 = SaisieDonnees.saisieDate();
		String conged2 = SaisieDonnees.saisieDate();
		String congef2 = SaisieDonnees.saisieDate();
		String perioded1 = SaisieDonnees.saisieDate();
		String periodef1 = SaisieDonnees.saisieDate();
		String perioded2 = SaisieDonnees.saisieDate();
		String periodef2 = SaisieDonnees.saisieDate();
		String perioded3 = SaisieDonnees.saisieDate();
		String periodef3 = SaisieDonnees.saisieDate();
		String certificationd = SaisieDonnees.saisieDate();
		String certificationf = SaisieDonnees.saisieDate();

		Table tableau = new Table(1).setBorder(new DoubleBorder(ColorConstants.BLACK, 2));
		tableau.setWidth(750).setFontSize(8);
		tableau.addCell(
				new Cell().add(new Paragraph("Election d�l�gu�s stagiaires :                                                  le " + election))
						.setBorder(new DoubleBorder(ColorConstants.BLACK, 2)).setBold());
		tableau.addCell(
				new Cell().add(new Paragraph("Rencontre avec le DC, le MF :                                                le " + rencontre))
						.setBorder(new DoubleBorder(ColorConstants.BLACK, 2)).setBold());
		tableau.addCell(
				new Cell().add(new Paragraph("Sensibilisation au D�veloppement Durable :                          le " + sensibilisation))
						.setBorder(new DoubleBorder(ColorConstants.BLACK, 2)).setBold());
		tableau.addCell(new Cell().add(new Paragraph("Atelier Accompagnement Vers l'Emploi,:                                le " + atelier))
				.setBorder(new DoubleBorder(ColorConstants.BLACK, 2)).setBold());
		tableau.addCell(
				new Cell().add(new Paragraph("Espaces de dialogues :                                  " + espaces))
						.setBorder(new DoubleBorder(ColorConstants.BLACK, 2)).setBold());
		tableau.addCell(new Cell()
				.add(new Paragraph("Cong�s                                                                                   du " + conged1
						+ " au " + congef1 + " - du " + conged2 + " au " + congef2))
				.setBorder(new DoubleBorder(ColorConstants.BLACK, 2)).setBold());
		tableau.addCell(new Cell()
				.add(new Paragraph(
						"P�riode en Entreprise 1 :                                                       du " + perioded1 + " au " + periodef1))
				.setBorder(new DoubleBorder(ColorConstants.BLACK, 2)).setBold());
		tableau.addCell(new Cell()
				.add(new Paragraph(
						"P�riode en Entreprise 2 :                                                       du " + perioded2 + " au " + periodef2))
				.setBorder(new DoubleBorder(ColorConstants.BLACK, 2)).setBold());
		tableau.addCell(new Cell()
				.add(new Paragraph(
						"P�riode en Entreprise 3 :                                                       du " + perioded3 + " au " + periodef3))
				.setBorder(new DoubleBorder(ColorConstants.BLACK, 2)).setBold());
		tableau.addCell(new Cell()
				.add(new Paragraph("Cetification :                                                                           du "
						+ certificationd + " au " + certificationf))
				.setBorder(new DoubleBorder(ColorConstants.BLACK, 2)).setBold());

		// ajout du tableau
		document.add(tableau.setTextAlignment(TextAlignment.LEFT).setFontSize(8));

		// ajout du GRN et de la signature
		document.add(new Paragraph(
				"GRN : 164                                                                                                                     "
				+ "                                                      Signature du formateur :").setFontSize(10).setBold());

		// cr�ation du qrcode
		try {
			qrCode(formateur);
		} catch (IOException | WriterException e1) {
			e1.printStackTrace();
		}
		// ajout du qrcode
		try {
			document.add(new Image(ImageDataFactory.create("C:\\ENV\\Ressources\\Images\\qrCode.png"))
					.setFixedPosition(550, 2).setBold());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		// fermeture du document
		document.close();

	}

	/**
	 * Cr�er le qrCode
	 * 
	 * @param nomFormateur le nom du formateur
	 * @throws IOException
	 * @throws WriterException
	 */
	public static void qrCode(String nomFormateur) throws IOException, WriterException {
		BitMatrix motCoder = null;
		FileOutputStream fluxDeSortie = null;
		motCoder = new QRCodeWriter().encode(nomFormateur, BarcodeFormat.QR_CODE, 75, 75);
		fluxDeSortie = new FileOutputStream(new File("C:\\ENV\\Ressources\\Images\\qrCode.png"));

		MatrixToImageWriter.writeToStream(motCoder, "png", fluxDeSortie);

		fluxDeSortie.close();
	}

}
