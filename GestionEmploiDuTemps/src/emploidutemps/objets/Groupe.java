package emploidutemps.objets;

import java.util.Scanner;

public class Groupe {

	private String intitule;
	private String debut;
	private String fin;
	private String salle;
	private String formateur;
	private String telFormateur;
	private String faxFormateur;

	public Groupe(String intitule1, String debut1, String fin1, String salle1, String formateur1, String telFormateur1, String faxFormateur1 ) {
		
		intitule1= intitule;
		debut1 = debut;
		fin1= fin;
		salle1= salle;
		formateur1 = formateur;
		telFormateur1 = telFormateur;
		faxFormateur1 = faxFormateur;
	}
	
  public void setIntitule(String intituleformation) {
	intitule=intituleformation;
    }

  public String GetIntitule() {
	return intitule;
    }
	

  public void setDebut(String dateDebut) {
	debut=dateDebut;
    }
	
  
  public String GetDebut() {
	  return debut;
  }
  
  public void setFin(String dateFin) {
		fin=dateFin;
	    }
		
	  
  public String GetFin() {
		  return fin;
	  }
  
  
  public void setSalle(String numSalle) {
		salle=numSalle;
	    }
		
	  
public String GetSalle() {
		  return salle;
	  }

public void setFormateur(String nomFormateur) {
	formateur=nomFormateur;
    }
	
  
public String GetTelFormateur() {
	  return formateur;
  }
  
public void setTelFormateur(String numFormateur) {
	telFormateur=numFormateur;
    }
	

public void setFaxFormateur(String numFaxFormateur) {
	faxFormateur=numFaxFormateur;
    }
	
  
public String GetFaxFormateur() {
	  return faxFormateur;
  }
  
public String GetFormateur() {
	  return formateur;
  }

	 
	  
	 
  }
  
  
