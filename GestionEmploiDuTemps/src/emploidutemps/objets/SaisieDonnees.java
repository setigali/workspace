package emploidutemps.objets;

import java.util.Scanner;

public class SaisieDonnees {
	static Scanner in = new Scanner(System.in);

	/**
	 * Saisie du groupe
	 * 
	 * @return	la chaine de caractere qui designe le groupe
	 */
	public static String saisieGroupe() {
		System.out.println("Veuillez saisir l'intitule de votre groupe");
		String groupe = in.nextLine();
		return groupe;
	}

	/**
	 * Saisie de la formation
	 * 
	 * @return	la chaine de caractere designant la formation
	 */
	public static String saisieFormation() {
		System.out.println("Veuillez saisir l'intitule de votre formation");
		String intitule = in.nextLine();
		intitule=intitule.toUpperCase();
		return intitule;
	}

	/**
	 * Saisie le debut de la formation
	 * 
	 * @return	la chaine de caractere designant le debut de la formation
	 */
	public static String saisieDebutFormation() {
		System.out.println("Veuillez saisir la date de debut de votre formation");
		String debut = in.next();
		in.nextLine();
		return debut;
	}

	/**
	 * Saisie la fin de la formation
	 * 
	 * @return	la chaine de caractere designant la fin de la formation
	 */
	public static String saisieFinFormation() {
		System.out.println("Veuillez saisir la date de fin de votre formation");
		String fin = in.next();
		in.nextLine();
		return fin;
	}

	/**
	 * Saisie du formateur
	 * 
	 * @return	la chaine de caractere designant le formateur
	 */
	public static String saisieFormateur() {
		System.out.println("Veuillez saisir le nom et pr�nom de votre formateur");
		String formateur = in.nextLine();
		return formateur;
	}
	
	/**
	 * Saisie des horaires
	 * 
	 * @return	un tableau de chaine de caractere designant les horaires
	 */
	public static String[] saisieHoraire() {
		System.out.println("Veuillez saisir les horaires du matin (les deux horaires du matin et les deux horaires de l'apr�s midi)");
		//cr�ation d'un tableau d'horaire pour les deux horaire du matin et les deux horaire de l'apres midi
		String[] horaires = new String[4];
		horaires[0] = in.next();	//horaire du d�but du matin
		in.nextLine();
		horaires[1] = in.next();	//horaire de fin du matin
		in.nextLine();
		horaires[2] = in.next();	//horaire du d�but de l'apres midi
		in.nextLine();
		horaires[3] = in.next();	//horaire de fin d'apres midi
		in.nextLine();
		
		
		return horaires;
	}
	
	/**
	 * Saisie du numero de telephone
	 * 
	 * @return	la chaine de caractere designant le numero de telephone
	 */
	public static String saisieTelephone() {
		System.out.println("Veuillez saisir le numero de telephone");
		String telephone = in.nextLine();
		return telephone;
	}
	
	/**
	 * Saisie du numero de fax
	 * 
	 * @return	la chaine de caractere designant le numero de fax
	 */
	public static String saisieFax() {
		System.out.println("Veuillez saisir le numero de fax");
		String fax = in.nextLine();
		return fax;
	}
	
	/**
	 * Saisie d'une date
	 * 
	 * @return	la chaine de caractere qui designe une date
	 */
	public static String saisieDate() {
		String date = in.nextLine();
		return date;
	}

}
