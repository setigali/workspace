import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class exoBetaQRCode {

	public static void main(String[] args) throws WriterException,IOException {
		String motAcoder = "Bonjour";
		BitMatrix motCoder = new QRCodeWriter().encode("Bonjour",BarcodeFormat.QR_CODE, 200, 200);

		
		String format = "png";
		String cheminDuFichier = "C:\\ENV\\Apis\\Api QR code";
		String nomDuFichier = "fichierqr";
		
		FileOutputStream fluxDeSortie = new FileOutputStream(new File(cheminDuFichier +"\\"+ nomDuFichier + "."+format));
		
		
		MatrixToImageWriter.writeToStream(motCoder,format,fluxDeSortie);
		fluxDeSortie.close();
	}

}
