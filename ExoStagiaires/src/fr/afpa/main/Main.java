package fr.afpa.main;

import java.util.ArrayList;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import fr.afpa.objets.Personne;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
	ArrayList <Personne> listeStagiaires = new ArrayList<Personne>();
		
	listeStagiaires.add(new Personne("GRAUWIN"," Guillaume","guillaume.grauwin@gmail.com","06-99-30-71-65"));
	listeStagiaires.add(new Personne("DEBAER", "Samuel","samuel.debaer@gmail.com","06-03-35-80-70"));
	listeStagiaires.add(new Personne("AKBARALY"," Fayaz", "fayaz.akbaraly@gmail.com","07-68-81-08-89"));
	listeStagiaires.add(new Personne("DZIAMSKI"," Nicolas",	"nicolas.dziamski@gmail.com","06-61-93-92-06"));
	listeStagiaires.add(new Personne("MASSAMBA", "Astelia","astelia.massamba@gmail.com"	,"03-20-47-31-58"));	
	listeStagiaires.add(new Personne("DORNE", "Julien",	"julien.dorne@laposte.net", "06-69-14-31-56"));	
	listeStagiaires.add(new Personne("OUAFFRA","Ali","ali.ouafrra@gmail.com	","06-49-82-75-34"));	
	listeStagiaires.add(new Personne("BEN AMARA", "Hakim","ha.benamara@gmail.com","06-02-12-83-52"));
	listeStagiaires.add(new Personne("GALI NGOTHE" ,"Seti","setiaissa@gmail.com","07- 88-19-25-87"));
	listeStagiaires.add(new Personne("ZENINA", "Alexandra","alex_zenina@yahoo.fr","06-44-11-21-08"));
    listeStagiaires.add(new Personne("DELACROIX" ,"Ga�tan","gaetandela@gmail.com","06 72 90 70 04"));									
	listeStagiaires.add(new Personne("LEON", "Charles"	,"charles.leon1997@gmail.com","06 10 62 23 86"));	
	listeStagiaires.add(new Personne("ZIMMER", "Marine","zimmer.marine@gmail.com","06 11 36 48 27"));		
	listeStagiaires.add(new Personne("ALIMA GARCIA", "St�phane ","garciastephan60@gmail.com","07 82 69 95 75"));
	listeStagiaires.add(new Personne("DIDOLLA", "David","daviddidolla@gmail.com","06 38 15 47 88"));
	
	
	Collections.sort(listeStagiaires);
	
	
	System.out.println();
	
	for (Personne personne : listeStagiaires) {
	//	personne.afficher();
	}
	
	HashMap<String, Personne> hashMapListeStagiaires2= new HashMap<String, Personne>();
	
	int index ;
	int poste=(int) 'A';
	
	while (listeStagiaires.size()>0) {
		index= new Random().nextInt(listeStagiaires.size());
		
		 {
			hashMapListeStagiaires2.put(""+(char)poste, listeStagiaires.get(index));
			poste++;
			
			listeStagiaires.remove(listeStagiaires.get(index));
		}
		
		
	}
	System.out.println();
	for (Map.Entry<String, Personne> personne : hashMapListeStagiaires2.entrySet()) {
		
		System.out.print(personne.getKey()+" ");
		personne.getValue().afficher();
	}
	
	}

}
