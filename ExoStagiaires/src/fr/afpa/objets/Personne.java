package fr.afpa.objets;
	
	public class Personne implements Comparable {

	
		private String nom;
		private String prenom;
		private String email;
		private String telephone;
		
	
		

		
		
		
			
			
		public Personne(String nom, String prenom, String email, String telephone) {
			super();
			this.nom = nom;
			this.prenom = prenom;
			this.email = email;
			this.telephone = telephone;
			
		
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getPrenom() {
			return prenom;
		}

		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getTelephone() {
			return telephone;
		}

		public void setTelephone(String telephone) {
			this.telephone = telephone;
		}

		
	
		
		public void afficher() {
			
			System.out.print("Nom :"+this.nom);
			System.out.print(" Prenom :"+this.prenom);
			System.out.print(" Mail :"+this.getEmail());
			System.out.println(" Telephone :"+this.telephone);
		
		}

		@Override
		public int compareTo(Object o) {
			
			return nom.compareTo(((Personne)o).getNom());
		}

		@Override
		public boolean equals(Object obj) {
			
			return this.nom.equals(((Personne)obj).getNom()) && this.prenom.equals(((Personne)obj).getPrenom());
		}
		
		

	}



